import React from 'react';
import { Form, Input, Button, Select, Row, Col } from 'antd';
import '../auth.scss';
import '../../../assets/img/logo_sm.png'

const layout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
};



export class Register extends React.Component {

    render() {
        return (
            <div>
                <Row>
                    <Col md={{ span: 16 }} lg={{ span: 16 }} sm={{ span: 24 }} className="welcome_img">
                        <img src={require('../../../assets/img/register.png')} />
                    </Col>

                    <Col md={{ span: 8 }} lg={{ span: 8 }} sm={{ span: 24 }} className="right_area">

                        <div className="logo_area">
                            <img src={require('../../../assets/img/logo_sm.png')} />
                        </div>

                        <div className="sign_in mt-20">Register  <span>Here  </span></div>

                        <Form {...layout} layout="vertical" ref={this.formRef} name="control-ref" >

                            <Form.Item name="E-mail address" label="E-mail address" >
                                <Input />
                            </Form.Item>

                            <Form.Item name="Password" label="Password" >
                                <Input.Password />
                            </Form.Item>

                            <Form.Item name="Confirm Password" label="Confirm Password" >
                                <Input.Password />
                            </Form.Item>

                            <Form.Item style={{ margin: ' 15px 0px' }} >
                                <Button className="k_btn">Register</Button>
                            </Form.Item>

                            <div className="first_time mt-30">
                                By logging in, You agree to the  <span>terms of services</span>
                            </div>

                        </Form>
                    </Col>
                </Row>
            </div>
        )
    }
}