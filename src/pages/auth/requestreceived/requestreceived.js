import React from 'react';

import './requestreceived.scss';
import { Button } from 'antd';
export class RequestReceived extends React.Component {
    render() {
        return (
            <div >
                <img className="verify_back" src={require('../../../assets/img/fullscreen.png')} />

                <div className="center_area">
                    <div className="body">
                        <img className="logo" src={require('../../../assets/img/logo_sm.png')} />

                        <div className="head">Request Received</div>
                        <div className="content1">Your request has been submitted.</div>
                        <div className="content"> Please monitor your email for the status of your request</div>
                        <div className="btn_area">
                            <div style={{ width: '200px' }}><Button className="k_btn">Close</Button></div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}
