import React from 'react';
import { Form, Input, Button, Select, Row, Col, Radio } from 'antd';
import '../auth.scss';
import '../../../assets/img/logo_sm.png'
const { Option } = Select;

const layout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
};



export class Registeration extends React.Component {

    render() {
        return (
            <div>
                <Row>
                    <Col md={{ span: 16 }} lg={{ span: 16 }} sm={{ span: 24 }} className="welcome_img">
                        <img src={require('../../../assets/img/register.png')} />
                    </Col>

                    <Col md={{ span: 8 }} lg={{ span: 8 }} sm={{ span: 24 }} className="right_area">

                        <div className="logo_area">
                            <img src={require('../../../assets/img/logo_sm.png')} />
                        </div>

                        <div className="sign_in mt-20">Register  <span>Here  </span></div>

                        <Form {...layout} layout="vertical" ref={this.formRef} name="control-ref" >


                            <Radio.Group >
                                <Radio value={1}>
                                    Affiliate
                             </Radio>
                                <Radio value={2}>
                                    Merchant
                              </Radio>
                            </Radio.Group>
                            <Form.Item name="E-mail address" label="E-mail address" >
                                <Input />
                            </Form.Item>

                            <Form.Item  >
                                <span style={{ display: 'flex' }}>
                                    <Form.Item name="First Name" label="First Name">
                                        <Input
                                            type="text"
                                            style={{ width: 150 }}
                                        />
                                    </Form.Item>
                                    &nbsp;
                                    &nbsp;
                                    &nbsp;
                                    <Form.Item name="Last Name" label="Last Name">
                                        <Input
                                            type="text"
                                            style={{ width: 150 }}
                                        />
                                    </Form.Item>

                                </span>
                            </Form.Item>

                            <Form.Item name="Phone Number" label="Phone Number" >
                                <Input type="tel" />
                            </Form.Item>

                            <div className="tellUs">Tell Us About Yourself : <span> (Optional) </span> </div>

                            <Form.Item  >
                                <span style={{ display: 'flex' }}>
                                    <Form.Item name="Bussiness Name" label="Bussiness Name">
                                        <Input
                                            type="text"
                                            style={{ width: 150 }}
                                        />
                                    </Form.Item>
                                    &nbsp;
                                    &nbsp;
                                    &nbsp;
                                    <Form.Item name="URL" label="URL">
                                        <Input
                                            type="text"
                                            style={{ width: 150 }}
                                        />
                                    </Form.Item>

                                </span>
                            </Form.Item>
                            <Form.Item style={{ margin: ' 15px 0px' }} >
                                <Button className="k_btn">Request Access</Button>
                            </Form.Item>

                            <div className="first_time mt-30">
                                By logging in, You agree to the  <span>terms of services</span>
                            </div>

                        </Form>
                    </Col>
                </Row>
            </div>
        )
    }
}