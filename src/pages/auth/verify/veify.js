import React from 'react';

import './verify.scss';
import { Button } from 'antd';
export class Verify extends React.Component {
    render() {
        return (
            <div >
                <img className="verify_back" src={require('../../../assets/img/fullscreen.png')} />

                <div className="center_area">
                    <div className="header">
                        <img src={require('../../../assets/img/logo_sm.png')} />
                    </div>
                    <div className="body">
                        <div className="head">Verify Your Email</div>
                        <div className="content">Check your email and follow the link to activate your account
                                            and complete registration!</div>
                        <div className="btn_area">
                            <div style={{ width: '200px' }}><Button className="k_btn">Resend</Button></div>
                            &nbsp;
                            <div style={{ width: '200px' }}><Button className="k_btn">Close</Button></div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}
