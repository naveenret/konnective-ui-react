import React from 'react';
import { Form, Input, Button, Select, Row, Col } from 'antd';
import '../auth.scss';
import '../../../assets/img/logo_sm.png'
import ReCAPTCHA from 'react-google-recaptcha';
import { LockOutlined } from '@ant-design/icons';

const layout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
};



export class Welcome extends React.Component {

    render() {
        return (
            <div>
                <Row>
                    <Col md={{ span: 16 }} lg={{ span: 16 }} sm={{ span: 24 }} className="welcome_img">
                        <img src={require('../../../assets/img/welcome.png')} />
                    </Col>
                    <Col md={{ span: 8 }} lg={{ span: 8 }} sm={{ span: 24 }} className="right_area">
                        <div className="logo_area">
                            <img src={require('../../../assets/img/logo_sm.png')} />
                        </div>
                        <div className="sign_in">Sign <span>In  </span></div>
                        <Form {...layout} layout="vertical" ref={this.formRef} name="control-ref" >
                            <Form.Item name="E-mail address" label="E-mail address" >
                                <Input />
                            </Form.Item>
                            <Form.Item name="Password" label="Password" >
                                <Input.Password />
                            </Form.Item>

                            <div className="forgot-pswd">Forgot Password?</div>
                            <Form.Item style={{ margin: ' 10px 0px' }} >
                                <ReCAPTCHA sitekey="Your client site key" />
                            </Form.Item>

                            <Form.Item style={{ margin: ' 15px 0px' }} >
                                <Button className="k_btn">Sign In</Button>
                                <div className="first_time">
                                    First Time Here? <span>Click Here to Sign Up</span>
                                </div>
                            </Form.Item>



                        </Form>
                    </Col>
                </Row>
            </div>
        )
    }
}