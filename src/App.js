import React from 'react';
import 'antd/dist/antd.css';
import logo from './logo.svg';
import './App.css';
import './App.scss';
import FirstPage from '@pages/firstPage/firstpage'
import { Welcome } from '@pages/auth/welcome/welcome';
import { Register } from './pages/auth/register/register';
import { Verify } from './pages/auth/verify/veify';
import { RequestReceived } from './pages/auth/requestreceived/requestreceived';
import { Registeration } from './pages/auth/registeration/registeration';
function App() {
  return (
    <div className="App">
      {/* <Welcome /> */}
      {/* <Register /> */}
      {/* <Verify /> */}
      {/* <RequestReceived /> */}
      <Registeration />
    </div>
  );
}

export default App;
